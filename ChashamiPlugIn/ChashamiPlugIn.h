//
//  ChashamiPlugIn.h
//  ChashamiPlugIn
//
//  Created by Lin Ehrippura Seruziu on 2010/5/31.
//  Copyright 2010 Eternal Wind. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <WebKit/WebKit.h>

@interface ChashamiPlugIn : NSObject {
	NSMutableArray * codeArray;
}
- (NSString *)js_searchTable:(NSString *)query;
- (void)generateCodeTable;
@end
