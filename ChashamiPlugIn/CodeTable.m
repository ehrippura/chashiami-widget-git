//
//  CodeTable.m
//  ChashamiPlugIn
//
//  Created by Lin Ehrippura Seruziu on 2010/5/31.
//  Copyright 2010 Eternal Wind. All rights reserved.
//

#import "CodeTable.h"


@implementation CodeTable

@synthesize code;
@synthesize word;

- (id)initWithCode:(NSString *)aCode word:(NSString *)aWord
{
	if (self = [super init]) {
		code = [aCode copy];
		word = [aWord copy];
		
		return self;
	}
	return nil;
}

- (void)dealloc {
	[code release];
	[word release];
	
	[super dealloc];
}

@end
