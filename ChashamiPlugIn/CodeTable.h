//
//  CodeTable.h
//  ChashamiPlugIn
//
//  Created by Lin Ehrippura Seruziu on 2010/5/31.
//  Copyright 2010 Eternal Wind. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface CodeTable : NSObject {
	NSString * code;
	NSString * word;
}

- (id)initWithCode:(NSString *)aCode word:(NSString *)aWord;

@property(readonly, nonatomic)NSString *code;
@property(readonly, nonatomic)NSString *word;

@end
