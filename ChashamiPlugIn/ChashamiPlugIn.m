//
//  ChashamiPlugIn.m
//  ChashamiPlugIn
//
//  Created by Lin Ehrippura Seruziu on 2010/5/31.
//  Copyright 2010 Eternal Wind. All rights reserved.
//

#import "ChashamiPlugIn.h"
#import "CodeTable.h"

NSString * const chashamiPrefixJSString = @"js_";

@implementation ChashamiPlugIn
#pragma mark -
#pragma mark initial codes
- (id)initWithWebView:(WebView *)webview
{
	if (self = [super init]) {
		codeArray = [[NSMutableArray alloc] init];
		[self generateCodeTable];
		return self;
	}
	return nil;
}

- (void)dealloc
{
	[codeArray release];
	[super dealloc];
}

- (void)windowScriptObjectAvailable:(WebScriptObject *)windowScriptObject;
{
	[windowScriptObject setValue:self forKey:@"ChashamiPlugIn"];
}

+ (BOOL)isSelectorExcludedFromWebScript:(SEL)aSelector {
	return !([NSStringFromSelector(aSelector) hasPrefix:chashamiPrefixJSString]);
}

+ (NSString *)webScriptNameForSelector:(SEL)aSelector
{
	NSString * selName = NSStringFromSelector(aSelector);
	
	if ([selName hasPrefix:chashamiPrefixJSString] && ([selName length] > [chashamiPrefixJSString length])) {
		return [[[selName substringFromIndex:[chashamiPrefixJSString length]] componentsSeparatedByString: @":"] objectAtIndex: 0];
	}
	return nil;
}

#pragma mark -
#pragma mark Javascript used code
- (NSString *)js_searchTable:(NSString *)query
{
	if ([query isEqualToString:@""])
		return @"";
	
	NSMutableString * result = [NSMutableString stringWithString:@""];
	
	for (CodeTable * aTable in codeArray) {
		
		if ([[query lowercaseString] isEqualToString:aTable.code]) {
			[result appendString:aTable.word];
			[result appendString:@" "];
		}
		else if ([query isEqualToString:aTable.word]) {
			[result appendString:aTable.code];
			[result appendString:@" "];
		}
	}
	
	if ([result isEqualToString:@""]) {
		return @"Nothing found!";
	}
	
	return result;
}

- (void)generateCodeTable
{
//	NSBundle * pluginBundle = [NSBundle bundleWithIdentifier:@"com.ehrippura.ChashamiPlugIn"];
	NSBundle * pluginBundle = [NSBundle bundleForClass:[self class]];
	NSString * tableFile = [pluginBundle pathForResource:@"table"
												  ofType:@"txt"];
	
	NSString * fileContent = [NSString stringWithContentsOfFile:tableFile
													   encoding:NSUnicodeStringEncoding
														  error:nil];
	
	NSString *code, *word;
	NSScanner * aScanner = [NSScanner scannerWithString:fileContent];
	NSCharacterSet * breakLine = [NSCharacterSet characterSetWithCharactersInString:@" \n"];
	
	while (![aScanner isAtEnd]) {
		if ([aScanner scanUpToString:@" " intoString:&code] &&
			[aScanner scanUpToCharactersFromSet:breakLine intoString:&word]) {
		
			CodeTable * aCodeTable = [[CodeTable alloc] initWithCode:code word:word];
			[codeArray addObject:aCodeTable];
			[aCodeTable release];
		}
	}
}
@end
